package redditbooru

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"

	"github.com/dghubble/sling"
)

const baseURL = "https://redditbooru.com/"


// List of sources to make it a bit easier to decipher
const (
	FiveToubunNoHanayome = 128
	ahoge = 49
	AnimeBlush = 83
	AnimeFeet = 60
	animegirlswithtowels = 66
	animehandbras = 88
	animelegs = 42
	animelegwear = 40
	animemaids = 73
	animemidriff = 106
	AnimeMILFS = 90
	animeponytails = 45
	animesidetails = 56
	AnimeSnowscapes = 57
	animewallpaper = 44
	AquaSama = 132
	araragi = 134
	araragigirls = 50
	awenime = 53
	awwcoholics = 51
	awwnime = 1
	AzureLane = 101
	AzurLewd = 102
	BakaChika = 72
	BigAnimeTiddies = 78
	bishounen = 25
	bowsette = 89
	CellsAtWork = 85
	chibi = 61
	chinadress = 39
	ChurchOfAkane = 124
	ChurchOfNakano = 125
	ChurchOfRikka = 123
	cutelittlefangs = 38
	DarlingInTheFranxx = 98
	ecchi = 9
	ElriosArtGallery = 65
	FGOfanart = 120
	fromis = 104
	fuJoJoshi = 36
	futanari = 108
	gamindustri = 79
	Ganbaruby = 93
	gao = 109
	GATE = 86
	GoblinSlayer = 103
	grailwhores = 100
	gunime = 24
	Hanamaru = 94
	hatsune = 28
	headpats = 43
	hentai = 107
	HentaiSource = 112
	HimeCut = 87
	homura = 21
	Honkers = 47
	ImaginarySliceOfLife = 105
	imouto = 13
	inumimi = 52
	itzy = 117
	k_on = 41
	kaguya_sama = 133
	KanMusu = 26
	KanMusuNights = 27
	kemonomimi = 20
	kitsunemimi = 7
	Konosuba = 75
	kpics = 58
	LegendaryMinalinsky = 97
	LewdAnimeGirls = 111
	LittleWitchAcademia = 127
	LoLFanArt = 129
	longhairedwaifus = 121
	LoveArrowShoot = 82
	LoveLive = 46
	megane = 48
	megumin = 113
	melanime = 8
	moescape = 15
	MoeStash = 54
	MoleisMoney = 95
	MonsterGirl = 29
	muchihentai = 110
	myouimina = 116
	nayeon = 114
	niconiconi = 81
	oddeye = 62
	OneTrueIchigo = 68
	OneTrueIdol = 33
	OneTrueKongou = 74
	OneTrueLittleSister = 76
	OneTrueRem = 64
	OneTrueTohsaka = 119
	Onodera = 30
	pantsu = 2
	patchuu = 17
	plsnobulli = 122
	pokeporn = 14
	pouts = 126
	rishkigal = 99
	Saber = 70
	SakurauchiRiko = 77
	shorthairedwaifus = 96
	SpiceandWolf = 135
	StardustCrusaders = 130
	streetmoe = 80
	sukebei = 22
	tanime = 84
	thecutestidol = 118
	TheForgottenIdol = 55
	TheRiceGoddess = 35
	thighdeology = 69
	TouRabu = 37
	tsunderes = 19
	TWGOK = 32
	twicemedia = 91
	twintails = 34
	TwoDeeArt = 23
	tzuyu = 115
	wholesomeyuri = 63
	Yousoro = 92
	yshirt = 71
	ZeroTwo = 67
	ZettaiRyouiki = 11
	ZombieLandSaga = 131
	AllSources = -1
)

//Sources is information about sources on redditbooru
type Sources []struct {
	Name    string `json:"name"`
	Title   string `json:"title"`
	Value   string `json:"value"`
	Checked bool   `json:"checked"`
}

//Results is search results
type Results []struct {
	ID          int         `json:"id"`
	ImageID     int         `json:"imageId"`
	CdnURL      string      `json:"cdnUrl"`
	Width       int         `json:"width"`
	Height      int         `json:"height"`
	Caption     string      `json:"caption"`
	SourceURL   string      `json:"sourceUrl"`
	Type        string      `json:"type"`
	SourceID    int         `json:"sourceId"`
	SourceName  string      `json:"sourceName"`
	PostID      int         `json:"postId"`
	Title       string      `json:"title"`
	Keywords    string      `json:"keywords"`
	DateCreated int         `json:"dateCreated"`
	ExternalID  string      `json:"externalId"`
	Score       string      `json:"score"`
	Visible     bool        `json:"visible"`
	UserID      int         `json:"userId"`
	UserName    string      `json:"userName"`
	Nsfw        bool        `json:"nsfw"`
	Thumb       string      `json:"thumb"`
	IdxInAlbum  interface{} `json:"idxInAlbum"`
	Age         int         `json:"age"`
}

//ResultsParams ...
type ResultsParams struct {
	imageURI   string // image url to search visually
	sources    string // "1,2,3"
	limit      int    // how many results to return
	afterID    int    // For paging; only fetches results after the image ID (currently buggy with albums). Default: null
	postID     int    //Returns all images for a specific post ID. Default: null
	externalID string // Returns all images for a specific reddit post ID. Default: null
	afterDate  int    // Unix timestamp
	user       string // username
	q          string // query string
}

//SourcesService ,,,
type SourcesService struct {
	sling *sling.Sling
}

//ResultsService ,,,
type ResultsService struct {
	sling *sling.Sling
}

// NewSourcesService returns a new SourcesService.
func NewSourcesService(httpClient *http.Client) *SourcesService {
	return &SourcesService{
		sling: sling.New().Client(httpClient).Base(baseURL),
	}
}

// NewResultsService returns a new ResultsService.
func NewResultsService(httpClient *http.Client) *ResultsService {
	return &ResultsService{
		sling: sling.New().Client(httpClient).Base(baseURL),
	}
}

// ReturnSources returns sources avaliable on redditbooru
func (s *SourcesService) ReturnSources() (Sources, *http.Response, error) {
	sources := new(Sources)
	var snErr error
	resp, err := s.sling.New().Path("sources/").ReceiveSuccess(sources)
	if err != nil {
		responseData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Printf("Error reading response body: %v\n", err)
		}

		fmt.Println("Results: ", string(responseData))
	}
	if resp.StatusCode != 200 {
		fmt.Printf("Query failed with Status Code: %v\n", resp.StatusCode)
	}
	return *sources, resp, snErr
}

// Search returns results avaliable on redditbooru
func (s *ResultsService) Search(params ResultsParams) (Results, *http.Response, error) {
	results := new(Results)
	var snErr error
	finalURL := fmt.Sprintf("sources=%s&limit=%v&user=%s&q=%s", params.sources, params.limit, url.QueryEscape(params.user), url.QueryEscape(params.q))
	fmt.Printf("FinalURL: %s\n", finalURL)
	resp, err := s.sling.New().Path("api/images/?").ReceiveSuccess(results)
	if err != nil {
		responseData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Printf("Error reading response body: %v\n", err)
		}

		fmt.Println("Results: ", string(responseData))
	}
	if resp.StatusCode != 200 {
		fmt.Printf("Query failed with Status Code: %v\n", resp.StatusCode)
	}
	return *results, resp, snErr
}

// Client to wrap services

// Client for saucenao
type Client struct {
	SourcesService *SourcesService
	ResultsService *ResultsService
}

// NewClient returns a new Client
func NewClient(httpClient *http.Client) *Client {
	return &Client{
		SourcesService: NewSourcesService(httpClient),
		ResultsService: NewResultsService(httpClient),
	}
}

/*func main() {
	client := NewClient(nil)
	rbSources, _, _ := client.SourcesService.ReturnSources()

	for i := range rbSources {
		fmt.Printf("%+v\n", rbSources[i])
	}

	var searchParams ResultsParams
	searchParams.limit = 5
	searchParams.q = "Shnee"
	searchParams.sources = string(AllSources)
	searchParams.user = ""

	results, _, _ := client.ResultsService.Search(searchParams)
	for i := range results {
		fmt.Printf("%+v\n", results[i])
	}
}
*/